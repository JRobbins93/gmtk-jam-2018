﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollidableObjectDisposer : MonoBehaviour
{
    [SerializeField]
    private CollidableObjectDisposalConfiguration m_disposalConfiguration;

    [SerializeField]
    private DisposedItemTracker m_disposedItemTracker;

    [SerializeField]
    private string m_itemName;
    public string ItemName { get { return m_itemName; } }

    private Rigidbody m_rigidbody;

    private Vector3 m_initialPosition;

    private float? m_timeToIgnoreFloor = null;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();

        m_initialPosition = transform.position;
    }

    private void Update()
    {
        if(m_rigidbody == null)
        {
            return;
        }

        if (m_rigidbody.IsSleeping())
        {
            if (m_timeToIgnoreFloor == null)
            {
                Vector3 toInitialPosition = m_initialPosition - transform.position;

                float toInitialPositionSquareMagnitude = toInitialPosition.sqrMagnitude;

                if (toInitialPositionSquareMagnitude > m_disposalConfiguration.DisplacementSquareMagnitudeBeforeDisposal)
                {
                    m_timeToIgnoreFloor = Time.time + m_disposalConfiguration.TimeAtRestBeforeDisposal;
                }
            }
        }
        else
        {
            m_timeToIgnoreFloor = null;
        }

        if (m_timeToIgnoreFloor != null && m_timeToIgnoreFloor <= Time.time && gameObject.layer != LayerMask.NameToLayer("IgnoreFloor")) 
        {
            SwitchToIgnoreFloorLayer();
        }

        if(transform.position.y < m_disposalConfiguration.YPositionToTriggerDisposal)
        {
            Destroy(gameObject);
        }
    }

    private void SwitchToIgnoreFloorLayer()
    {
        m_disposedItemTracker.OnItemDisposed(this);
        gameObject.layer = LayerMask.NameToLayer("IgnoreFloor");
    }

}
