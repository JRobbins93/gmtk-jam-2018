﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class PlayerCollisionConfiguration : ScriptableObject
{
    [SerializeField]
    private float m_appliedForceMultiplier = 2.5f;
	public float AppliedForceMultiplier { get { return m_appliedForceMultiplier; } }
}
