﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public static HUD Instance;

    [SerializeField]
    private DisposedItemTracker m_disposedItemTracker;

    [SerializeField]
    private Text m_scoreText;

    [SerializeField]
    private GameObject m_settingsPanel;

    [SerializeField]
    private GameObject m_endGamePanel;

    [SerializeField]
    private GameObject m_titleScreenPanel;

    [SerializeField]
    private GameObject m_victoryScreen;

    [SerializeField]
    private Text m_endGameTotalText;

    [SerializeField]
    private GameObject[] m_objectsToDeactiveOnEndGame;

    [SerializeField]
    private GameObject[] m_shoppingListDashes;

    private void Awake()
    {
        Instance = this;

        m_settingsPanel.SetActive(false);
        m_endGamePanel.SetActive(false);
        m_victoryScreen.SetActive(false);

        foreach(GameObject dash in m_shoppingListDashes)
        {
            dash.SetActive(false);
        }

        m_titleScreenPanel.SetActive(true);
    }

    private void Update()
    {
        m_scoreText.text = m_disposedItemTracker.DisposedItemsValueSum.ToString();

        if (Input.GetKeyDown(KeyCode.Return) && m_victoryScreen.activeSelf)
        {
            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("SuperMarket_00");
            UnityEngine.SceneManagement.SceneManager.LoadScene("SuperMarket_00", UnityEngine.SceneManagement.LoadSceneMode.Additive);
            m_victoryScreen.SetActive(false);
            Time.timeScale = 1.0f;
        }
    }

    public void OnPauseButtonClicked()
    {
        m_settingsPanel.SetActive(!m_settingsPanel.activeSelf);
        Time.timeScale = 1.0f - Time.timeScale;
    }

    public void OnSettingsButtonClicked()
    {
        m_settingsPanel.SetActive(!m_settingsPanel.activeSelf);
        Time.timeScale = 1.0f - Time.timeScale;
    }

    public void OnExitPauseButtonClicked()
    {
        m_settingsPanel.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void OnSoundOnButtonClicked()
    {
        AudioListener.pause = false;
    }

    public void OnSoundOffButtonClicked()
    {
        AudioListener.pause = true;
    }

    public void OnDismissEndGameButtonPressed()
    {
        m_endGamePanel.SetActive(false);
        m_victoryScreen.SetActive(true);
    }

    public void OnStartButtonClicked()
    {
        m_titleScreenPanel.SetActive(false);
        UnityEngine.SceneManagement.SceneManager.LoadScene("SuperMarket_00", UnityEngine.SceneManagement.LoadSceneMode.Additive);
        GetComponent<Camera>().enabled = false;
    }

    public void OnGameEnded()
    {
        Time.timeScale = 0.0f;
        m_endGamePanel.SetActive(true);
        m_endGameTotalText.text = m_disposedItemTracker.DisposedItemsValueSum.ToString();
        

        foreach(GameObject go in m_objectsToDeactiveOnEndGame)
        {
            go.SetActive(false);
        }
    }

    public void OnShoppingItemCollected(string itemName)
    {
        int shoppingListDashIndex = -1;

        switch(itemName)
        {
            case "Strawberries":
                shoppingListDashIndex = 0;
                break;
            case "Milk":
                shoppingListDashIndex = 1;
                break;
            case "Bread":
                shoppingListDashIndex = 2;
                break;
            case "Peanut Butter":
                shoppingListDashIndex = 3;
                break;
            case "Rice":
                shoppingListDashIndex = 4;
                break;
            case "Sugar Shurikens":
                shoppingListDashIndex = 5;
                break;
            default:
                Debug.LogWarningFormat("No shopping list item supported with name - {0}", itemName);
                return;
        }

        m_shoppingListDashes[shoppingListDashIndex].SetActive(true);
    }
}
