﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class CameraPositionerConfiguration : ScriptableObject
{
    [SerializeField]
    private Vector3 m_positionOffset;
    public Vector3 PositionOffset { get { return m_positionOffset; } }

    [SerializeField]
    private Vector3 m_rotationToApply;
    public Quaternion RotationToApply {  get { return Quaternion.Euler(m_rotationToApply); } }
}
