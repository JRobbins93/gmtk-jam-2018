﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class DisposedItemTracker : ScriptableObject {

    [System.Serializable]
    private struct ItemValuePair
    {
        public string m_itemName;
        public int m_value;
    }

    [SerializeField]
    private ItemValuePair[] m_itemValuePairs;

    private Dictionary<string, int> m_disposedItems;
   
    public int DisposedItemsValueSum
    {
        get
        {
            int sum = 0;

            foreach (var entry in m_disposedItems)
            {
                int? itemValue = null;

                foreach (ItemValuePair pair in m_itemValuePairs)
                {
                    if (pair.m_itemName == entry.Key)
                    {
                        itemValue = pair.m_value;
                        break;
                    }
                }

                sum += entry.Value * itemValue.Value;
            }

            return sum;
        }
    }

    public void OnItemDisposed(CollidableObjectDisposer item)
    {
        int? itemValue = null;

        foreach(ItemValuePair pair in m_itemValuePairs)
        {
            if (pair.m_itemName == item.ItemName)
            {
                itemValue = pair.m_value;
                break;
            }
        }

        if(itemValue == null)
        {
            Debug.LogWarningFormat("No known item with name: {0}", item.ItemName);
            return;
        }

        if (!m_disposedItems.ContainsKey(item.ItemName))
        {
            m_disposedItems.Add(item.ItemName, 1);
        }
        else
        {
            m_disposedItems[item.ItemName]++;
        }
    }

    private void OnEnable()
    {
        m_disposedItems = new Dictionary<string, int>();
    }
}
