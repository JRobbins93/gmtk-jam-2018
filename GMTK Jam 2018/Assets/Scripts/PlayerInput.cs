﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    private const string c_horizontalAxisName = "Horizontal";
    private const string c_verticalAxisName = "Vertical";

    public Vector2 GetInputVector()
    {
        float horizontalInput = Input.GetAxis(c_horizontalAxisName);
        float verticalInput = Input.GetAxis(c_verticalAxisName);

        return new Vector2(horizontalInput, verticalInput);
    }
}
