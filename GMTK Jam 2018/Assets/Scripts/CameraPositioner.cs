﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositioner : MonoBehaviour
{
    [SerializeField]
    CameraPositionerConfiguration m_positionerConfiguration;

    [SerializeField]
    private Transform m_transformToFollow;

    private void Update()
    {
        Vector3 newPosition = m_transformToFollow.position + m_positionerConfiguration.PositionOffset;
        transform.position = newPosition;

        transform.rotation = m_positionerConfiguration.RotationToApply;
    }
}
