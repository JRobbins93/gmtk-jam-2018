﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField]
    private PlayerInput m_playerInput;

    [SerializeField]
    private PlayerMovementConfiguration m_movementConfiguration;

    private Rigidbody m_rigidBody;

    private void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Vector3 currentVelocity = m_rigidBody.velocity;
        float currentVelocitySquareMagnitude = currentVelocity.sqrMagnitude;

        if(currentVelocitySquareMagnitude < m_movementConfiguration.MaxVelocitySquareMagnitude)
        {
            Vector2 inputVector = m_playerInput.GetInputVector();

            Vector3 forceVector = new Vector3(inputVector.x, 0.0f, inputVector.y);
            forceVector *= m_movementConfiguration.ForceMagnitude;

            Vector3 torqueVector = new Vector3(inputVector.y, 0.0f, -inputVector.x);

            m_rigidBody.AddForce(forceVector);
            m_rigidBody.AddTorque(torqueVector);
        }
    }
}
