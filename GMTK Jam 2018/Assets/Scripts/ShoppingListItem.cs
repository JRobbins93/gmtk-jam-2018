﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoppingListItem : MonoBehaviour {

    [SerializeField]
    private string m_itemName;
    public string ItemName { get { return m_itemName; } }
}
