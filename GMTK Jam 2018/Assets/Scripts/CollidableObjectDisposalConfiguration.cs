﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class CollidableObjectDisposalConfiguration : ScriptableObject
{
    [SerializeField]
    private float m_timeAtRestBeforeDisposal = 1.0f;
    public float TimeAtRestBeforeDisposal {  get { return m_timeAtRestBeforeDisposal; } }

    [SerializeField]
    private float m_displacementSquareMagnitudeBeforeDisposal = 1.5f;
    public float DisplacementSquareMagnitudeBeforeDisposal { get { return m_displacementSquareMagnitudeBeforeDisposal; } }

    [SerializeField]
    private float m_yPositionToTriggerDisposal = -100.0f;
    public float YPositionToTriggerDisposal {  get { return m_yPositionToTriggerDisposal; } }
}
