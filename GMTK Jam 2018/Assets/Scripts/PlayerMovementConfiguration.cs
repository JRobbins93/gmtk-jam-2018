﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class PlayerMovementConfiguration : ScriptableObject
{
    [SerializeField]
    private float m_forceMagnitude = 1.0f;
    public float ForceMagnitude { get { return m_forceMagnitude; } }

    [SerializeField]
    private float m_maxVelocitySquareMagnitude = 100.0f;
    public float MaxVelocitySquareMagnitude {  get { return m_maxVelocitySquareMagnitude; } }
}
