﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionHandler : MonoBehaviour
{
    [SerializeField]
    private PlayerCollisionConfiguration m_collisionConfiguration;

    [SerializeField]
    private AudioClip[] m_collisionAudioClips;

    private AudioSource m_audioSource;

    private void Awake()
    {
        m_audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Floor"))
        {
            return;
        }

        ContactPoint contact = collision.contacts[0];

        float appliedForceMagnitude = m_collisionConfiguration.AppliedForceMultiplier * collision.relativeVelocity.magnitude;

        Vector3 appliedForce = -contact.normal * appliedForceMagnitude;

        if (collision.rigidbody != null)
        {
            collision.rigidbody.AddForceAtPosition(appliedForce, contact.point);
        }

        if (collision.gameObject.tag == "Exit")
        {
            HUD.Instance.OnGameEnded();
        }

        if(collision.gameObject.tag == "Collidible")
        {
            int audioClipIndex = Random.Range(0, m_collisionAudioClips.Length);
            AudioClip audioClip = m_collisionAudioClips[audioClipIndex];
            m_audioSource.PlayOneShot(audioClip);
        }

        if(collision.gameObject.tag == "ShoppingItem")
        {
            var item = collision.gameObject.GetComponent<ShoppingListItem>();
            HUD.Instance.OnShoppingItemCollected(item.ItemName);
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Collectible")
        {
            Destroy(other.gameObject);
        }
    }
}
